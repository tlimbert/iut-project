'use strict';

const nodemailer = require('nodemailer');
const {Service} = require('schmervice');

module.exports = class MailerService extends Service {

    async send(mail, subject, content) {

        const transporter = nodemailer.createTransport({
            host: 'smtp.ethereal.email',
            port: 587,
            secure: false,
            auth: {
                user: 'ladarius23@ethereal.email', // generated ethereal user
                pass: 'SE6YsGMPgsTz2mRKYM', // generated ethereal password
            },
        });

        const info = await transporter.sendMail({
            from: '"Fred Foo 👻" <foo@example.com>',
            to: mail,
            subject: subject,
            text: content
        });
    }

    async warn(movie){
        const {User} = this.server.models();
        const users = await User.query().select('mail');
        for (let user of users) {
            this.send(user.mail, 'New movie added', 'We\'ve just added a new movie : ' + movie.title + '. Look at it !');
        }
    }

    async warnUpdate(movie){
        const {User} = this.server.models();
        const {Watchlist} = this.server.models();
        let usersMovie = await Watchlist.query().select('user').where({'movie': movie.id});
        usersMovie = usersMovie.map(watchlist => watchlist.user);
        const users = await User.query().select('mail').whereIn('id', usersMovie);
        for (let user of users) {
            this.send(user.mail, 'Movie updated', 'We\'ve just updated a movie of your watchlist : ' + movie.title + '. Look at it !');
        }
    }
};
