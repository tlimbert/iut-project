'use strict';

const {Service} = require('schmervice');

module.exports = class WatchlistService extends Service {

    create(elem) {
        const {Watchlist} = this.server.models();
        return Watchlist.query().insert(elem);
    }

    async get(userId) {
        const {Watchlist} = this.server.models();
        const result = await Watchlist.query().select('movie').where({user: userId});
        return result;
    }

    async delete(payload) {
        const {Watchlist} = this.server.models();
        return await Watchlist.query().delete().where({user: payload.userId, movie: payload.movieId});
    }
};
