'use strict';

const Joi = require('joi');
const {Model} = require('schwifty');
const encrypt = require('@tlimbert/iut-encrypt');

module.exports = class User extends Model {

    static get tableName() {
        return 'user';
    }

    static get joiSchema() {
        return Joi.object({
            id: Joi.number().integer().greater(0),
            firstname: Joi.string().min(3).example('John').description('Firstname of the user'),
            lastname: Joi.string().min(3).example('Doe').description('Lastname of the user'),
            password: Joi.string().min(8).example('DG12#jdS')
                //.regex(new RegExp('^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$'))
                .description('Password of the user'),
            username: Joi.string().min(3).example('Josh45853').description('Username of the user'),
            mail: Joi.string().min(3).example('Josh45853@example.com')
                .regex(new RegExp('(?:[a-z0-9!#$%&\'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&\'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\\])'))
                .description('Mail address of the user'),
            scope: Joi.object(),
            createdAt: Joi.date(),
            updatedAt: Joi.date()
        });
    }

    static get jsonAttributes() {
        return ['scope'];
    }

    $beforeInsert(queryContext) {
        this.password = encrypt.sha1(this.password);
        this.updatedAt = new Date();
        this.createdAt = this.updatedAt;
        if (typeof this.scope === 'undefined')
            this.scope = [];
        if (!this.scope.includes('user'))
            this.scope.push('user');
    }

    $beforeUpdate(opt, queryContext) {
        this.updatedAt = new Date();
    }

};
