'use strict';

const {Service} = require('schmervice');

module.exports = class MovieService extends Service {

    async create(movie) {
        const {Movie} = this.server.models();
        const id = await Movie.query().insertAndFetch(movie);
        const {mailerService} = this.server.services();
        mailerService.warn(movie);
        return id;
    }

    async getAll() {
        const {Movie} = this.server.models();
        return await Movie.query().select();
    }

    async getOne(id) {
        const {Movie} = this.server.models();
        return await Movie.query().select().where({id: id});
    }

    async delete(id) {
        const {Movie} = this.server.models();
        const {Watchlist} = this.server.models();
        await Watchlist.query().delete().where({movie: id});
        return await Movie.query().deleteById(id);
    }

    async update(movie) {
        const {Movie} = this.server.models();

        if((await Movie.query().select().where({id: movie.id})).length > 0){
            if(typeof movie.title !== 'undefined')
                await Movie.query().update({title: movie.title}).where({id: movie.id});
            if(typeof movie.description !== 'undefined')
                await Movie.query().update({description: movie.description}).where({id: movie.id});
            if(typeof movie.releaseDate !== 'undefined')
                await Movie.query().update({releaseDate: movie.releaseDate}).where({id: movie.id});
            if(typeof movie.director !== 'undefined')
                await Movie.query().update({director: movie.director}).where({id: movie.id});

            const {mailerService} = this.server.services();
            mailerService.warnUpdate(movie);

            return true;
        }
        return false;
    }
};
