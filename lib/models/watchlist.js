'use strict';

const Joi = require('joi');
const { Model } = require('schwifty');

module.exports = class Watchlist extends Model {

    static get tableName() {
        return 'watchlist';
    }

    static get joiSchema() {
        return Joi.object({
            movie: Joi.number().integer().greater(0),
            user: Joi.number().integer().greater(0),
            addedOn: Joi.date()
        });
    }

    $beforeInsert(queryContext) {
        this.addedOn = new Date();
    }

    $beforeUpdate(opt, queryContext) {
    }

};
