'use strict';

const {Service} = require('schmervice');
const encrypt = require('@tlimbert/iut-encrypt');
const Jwt = require('@hapi/jwt');

module.exports = class UserService extends Service {

    async create(user) {
        const {User} = this.server.models();
        const check = await User.query().select().where({username: user.username}).orWhere({mail: user.mail});

        if (check.length === 0) {
            const id = await User.query().insertAndFetch(user);
            const {mailerService} = this.server.services();
            mailerService.send(user.mail, 'Welcome', 'Welcome on my HAPI !');
            return id;
        }
        return -1;
    }

    async getAll() {
        const {User} = this.server.models();
        return await User.query().select();
    }

    async getOne(id) {
        const {User} = this.server.models();
        return await User.query().findById(id);
    }

    async delete(id) {
        const {User} = this.server.models();
        const {Watchlist} = this.server.models();
        Watchlist.query().delete().where({user: id});
        return await User.query().deleteById(id);
    }

    async update(user) {
        const {User} = this.server.models();

        if ((await User.query.select().where({id: user.id})).length > 0) {
            if (typeof user.firstName !== 'undefined')
                await User.query().update({firstName: user.firstName}).where({id: user.id});
            if (typeof user.lastName !== 'undefined')
                await User.query().update({lastName: user.lastName}).where({id: user.id});
            if (typeof user.username !== 'undefined')
                await User.query().update({username: user.username}).where({id: user.id});
            if (typeof user.password !== 'undefined')
                await User.query().update({password: user.password}).where({id: user.id});
            if (typeof user.mail !== 'undefined')
                await User.query().update({mail: user.mail}).where({id: user.id});

            return true;
        }
        return false;
    }

    async authentication(data) {
        const {User} = this.server.models();

        let user = await User.query().select('password', 'scope').where({mail: data.mail}).limit(1);
        let hash = encrypt.sha1(data.password);

        if (user.length > 0 &&  hash === user[0].password) {
            return Jwt.token.generate(
                {
                    aud: 'urn:audience:iut',
                    iss: 'urn:issuer:iut',
                    email: data.mail,
                    scope: JSON.parse(JSON.stringify(user[0].scope))
                },
                {
                    key: 'random_string', // La clé qui est définit dans lib/auth/strategies/jwt.js
                    algorithm: 'HS512'
                },
                {
                    ttlSec: 14400 // 4 hours
                }
            );
        } else {
            return null;
        }

    }
};
