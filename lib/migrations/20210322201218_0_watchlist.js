'use strict';

exports.up = async function (knex) {
    await knex.schema.createTable('watchlist', (table) => {
        table.integer('user').notNullable();
        table.integer('movie').notNullable();
        table.dateTime('addedOn').notNullable();
        table.primary(['user', 'movie'], 'pk');
    });
};

exports.down = async function (knex) {
    await knex.schema.dropTableIfExists('watchlist');
};
