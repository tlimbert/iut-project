# iut-project

API réalisée dans le cadre d'un projet de licence professionnelle en développement web à l'Université du Limousin.

## Installation
Pour installer cette application, commencer par installer les dépendances avec la commande suivante : 
```shell
npm install
```

## Variables

Les variables d'environnement sont définies dans le fichier ```.env```, elles concernent la connexion à la base de données. Veuillez donc les modifier en suivant ces indications :
 - ```DB_CLIENT='mysql'``` : type de base de données (MySql, MSSQL, Postgre, etc.)
 - ```DB_HOST='0.0.0.0'``` : adresse IP de l'hôte
 - ```DB_DATABASE='iut-project'``` : nom de la base de données
 - ```DB_USER='root'``` : utilisateur pour se connecter à la base de données (attention, il doit posséder tous les droits sur la base)
 - ```DB_PASSWORD='passwd'``` : mot de passe de l'utilisateur

## Base de données

Pour créer la table de données, lancer la commande suivante : 
```shell
npx knex migrate:latest
```

## Lancement

Pour démarrer l'api, lancer la commande suivante : 
```shell
npm start
```

## Test

Si vous souhaitez vérifier que votre installation fonctionne, vous pouvez lancer les tests intégrés à l'application.
Avant ça, vous devez cependant créer un utilisateur dans la table ```user```, en respectant ces conditions : 
- mail : ```admin@gmail.com```
- password : ```70352f41061eda4ff3c322094af068ba70c3b38b```
- scope : ```["admin","user"]```

Vous pouvez ensuite lancer les tests avec la commande suivante :
```shell
lab
```

## Félicitions : votre installation est terminée !


