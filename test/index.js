'use strict';

const Lab = require('@hapi/lab');
const { expect } = require('@hapi/code');
const { afterEach, beforeEach, describe, it } = exports.lab = Lab.script();
const { deployment } = require('../server/index');

let token;
let userId;
let movieId;

describe('CHECK INSTALLATION', () => {
    let server;

    beforeEach(async () => {
        server = await deployment();
    });

    afterEach(async () => {
        await server.stop();
    });

    it('responds with 200', async () => {
        const res = await server.inject({
            method: 'POST',
            url: '/user/login',
            payload: {
                mail: 'admin@gmail.com',
                password: '00000000'
            }
        });
        token = res.result.token;
        expect(res.statusCode).to.equal(200);
    });

    it('responds with 200', async () => {
        const res = await server.inject({
            method: 'POST',
            url: '/user',
            payload: {
                firstname: 'Jack',
                lastname: 'Bauer',
                password: '#24hoursRescuePresident!',
                username: 'jack_b',
                mail: 'limbel36@gmail.com'
            },
            headers: {
                Authorization: token
            }
        });
        expect(res.statusCode).to.equal(200);
        userId = res.result.id;
    });

    it('responds with 200', async () => {
        const res = await server.inject({
            method: 'GET',
            url: '/user/all',
            headers: {
                Authorization: token
            }
        });
        expect(res.statusCode).to.equal(200);
    });

    it('responds with 200', async () => {
        const res = await server.inject({
            method: 'GET',
            url: '/user/' + userId,
            headers: {
                Authorization: token
            }
        });
        expect(res.statusCode).to.equal(200);
    });

    it('responds with 200', async () => {
        const res = await server.inject({
            method: 'POST',
            url: '/movie',
            payload: {
                title: 'Jack',
                description: 'Bauer',
                releaseDate: new Date(),
                director: 'jack_b'
            },
            headers: {
                Authorization: token
            }
        });
        movieId = res.result.id;
        expect(res.statusCode).to.equal(200);
    });

    it('responds with 200', async () => {
        const res = await server.inject({
            method: 'PATCH',
            url: '/movie',
            payload: {
                id: movieId,
                title: 'Jackdfs'
            },
            headers: {
                Authorization: token
            }
        });
        expect(res.statusCode).to.equal(200);
    });

    it('responds with 200', async () => {
        const res = await server.inject({
            method: 'GET',
            url: '/movie/all',
            headers: {
                Authorization: token
            }
        });
        expect(res.statusCode).to.equal(200);
    });

    it('responds with 200', async () => {
        const res = await server.inject({
            method: 'GET',
            url: '/movie/' + movieId,
            headers: {
                Authorization: token
            }
        });
        expect(res.statusCode).to.equal(200);
    });

    it('responds with 200', async () => {
        const res = await server.inject({
            method: 'POST',
            url: '/watchlist',
            payload: {
                user: 1,
                movie: movieId
            },
            headers: {
                Authorization: token
            }
        });
        expect(res.statusCode).to.equal(200);
    });

    it('responds with 200', async () => {
        const res = await server.inject({
            method: 'GET',
            url: '/watchlist/' + 1,
            headers: {
                Authorization: token
            }
        });
        expect(res.statusCode).to.equal(200);
    });

    it('responds with 200', async () => {
        const res = await server.inject({
            method: 'DELETE',
            url: '/watchlist/' + 1 + '/' + movieId,
            headers: {
                Authorization: token
            }
        });
        expect(res.statusCode).to.equal(200);
    });

    it('responds with 200', async () => {
        const res = await server.inject({
            method: 'DELETE',
            url: '/movie/' + movieId,
            headers: {
                Authorization: token
            }
        });
        expect(res.statusCode).to.equal(200);
    });

    it('responds with 200', async () => {
        const res = await server.inject({
            method: 'DELETE',
            url: '/user/' + userId,
            headers: {
                Authorization: token
            }
        });
        expect(res.statusCode).to.equal(200);
    });
});
