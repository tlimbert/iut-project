'use strict';

const Joi = require('joi');
const Boom = require('boom');

module.exports = [
    {
        method: 'POST',
        path: '/movie',
        options: {
            tags: ['api'],
            validate: {
                payload: Joi.object({
                    title: Joi.string().min(3).example('Star wars').description('Title of the movie').required(),
                    description: Joi.string().min(3).example('A long story far far away').description('Description of the movie').required(),
                    releaseDate: Joi.date().example(new Date('10-13-1999')).description('Date when was release the movie').required(),
                    director: Joi.string().min(3).example('George Lucas').description('Director of the movie').required()
                })
            },
            auth: false
        },
        handler: async (request, h) => {
            const {movieService} = request.services();

            return await movieService.create(request.payload);
        }
    },
    {
        method: 'GET',
        path: '/movie/{id}',
        options: {
            auth: {
                scope: ['admin', 'user']
            },
            tags: ['api']
        },
        handler: async (request, h) => {
            const {movieService} = request.services();

            return await movieService.getOne(request.params.id);
        }
    },
    {
        method: 'GET',
        path: '/movie/all',
        options: {
            auth: {
                scope: ['admin', 'user']
            },
            tags: ['api']
        },
        handler: async (request, h) => {
            const {movieService} = request.services();

            return await movieService.getAll();
        }
    },
    {
        method: 'DELETE',
        path: '/movie/{id}',
        options: {
            tags: ['api'],
            auth: {
                scope: ['admin']
            }
        },
        handler: async (request, h) => {
            const {movieService} = request.services();

            return await movieService.delete(request.params.id);
        }
    },
    {
        method: 'PATCH',
        path: '/movie',
        options: {
            tags: ['api'],
            auth: {
                scope: ['admin']
            },
            validate: {
                payload: Joi.object({
                    id: Joi.number().integer().greater(0).required(),
                    title: Joi.string().min(3).example('Star wars').description('Title of the movie'),
                    description: Joi.string().min(3).example('A long story far far away').description('Description of the movie'),
                    releaseDate: Joi.date().example(new Date('10-13-1999')).description('Date when was release the movie'),
                    director: Joi.string().min(3).example('George Lucas').description('Director of the movie')
                })
            }
        },
        handler: async (request, h) => {
            const {movieService} = request.services();
            const res = await movieService.update(request.payload);
            return res ? h.response({text: 'Movie updated', done: true}).code(200) : Boom.badData('Movie not found!');
        }
    }
];
