'use strict';

const Joi = require('joi');
const Boom = require('boom');
const Jwt = require('@hapi/jwt');

module.exports = [
    {
        method: 'POST',
        path: '/watchlist',
        options: {
            tags: ['api'],
            validate: {
                payload: Joi.object({
                    user: Joi.number().integer().required(),
                    movie: Joi.number().integer().required()
                })
            },
            auth: false
        },
        handler: async (request, h) => {
            const token = request.headers.authorization;
            const {watchlistService} = request.services();
            const {userService} = request.services();
            const user = await userService.getOne(request.payload.user);
            const mail = Jwt.token.decode(token.split(" ")[1]).decoded.payload.email;
            if(mail !== user.mail)
                return Boom.unauthorized("You cannot add movie in the watchlist of someone else!");
            let movies = await watchlistService.get(request.payload.user);
            movies = movies.filter(item => item.movie === request.payload.movie);
            if(movies.length > 0)
                return Boom.badData("This movie is already in your watchlist")
            return await watchlistService.create(request.payload);
        }
    },
    {
        method: 'GET',
        path: '/watchlist/{userId}',
        options: {
            auth: {
                scope: ['admin', 'user']
            },
            tags: ['api']
        },
        handler: async (request, h) => {
            const token = request.headers.authorization;
            const {watchlistService} = request.services();
            const {userService} = request.services();
            const user = await userService.getOne(request.params.userId);
            const mail = Jwt.token.decode(token.split(" ")[1]).decoded.payload.email;
            if(mail !== user.mail)
                return Boom.unauthorized("You cannot get the watchlist of someone else!");
            return await watchlistService.get(request.params.userId);
        }
    },
    {
        method: 'DELETE',
        path: '/watchlist/{userId}/{movieId}',
        options: {
            tags: ['api'],
            auth: {
                scope: ['admin']
            }
        },
        handler: async (request, h) => {
            const token = request.headers.authorization;
            const {watchlistService} = request.services();
            const {userService} = request.services();
            const user = await userService.getOne(request.params.userId);
            const mail = Jwt.token.decode(token.split(" ")[1]).decoded.payload.email;
            if(mail !== user.mail)
                return Boom.unauthorized("You cannot delete a movie of the watchlist of someone else!");
            return await watchlistService.delete(request.params);
        }
    }
];
