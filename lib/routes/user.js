'use strict';

const Joi = require('joi');
const Boom = require('boom');

module.exports = [
    {
        method: 'POST',
        path: '/user',
        options: {
            tags: ['api'],
            validate: {
                payload: Joi.object({
                    firstname: Joi.string().min(3).example('John').description('Firstname of the user').required(),
                    lastname: Joi.string().min(3).example('Doe').description('Lastname of the user').required(),
                    password: Joi.string().min(8).example('DG12#jdS')
                        //.regex(new RegExp('^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$'))
                        .description('Password of the user').required(),
                    username: Joi.string().min(3).example('Josh45853').description('Username of the user').required(),
                    mail: Joi.string().min(3).example('Josh45853@example.com')
                        .regex(new RegExp('(?:[a-z0-9!#$%&\'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&\'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\\])'))
                        .description('Mail address of the user').required()
                })
            },
            auth: false
        },
        handler: async (request, h) => {
            const {userService} = request.services();
            const res = await userService.create(request.payload);
            return res === -1 ? Boom.badData('Error: mail or username already used') : res;
        }
    },
    {
        method: 'GET',
        path: '/user/all',
        options: {
            auth: {
                scope: ['user']
            },
            tags: ['api']
        },
        handler: async (request, h) => {
            const {userService} = request.services();
            return await userService.getAll();
        }
    },
    {
        method: 'GET',
        path: '/user/{id}',
        options: {
            auth: {
                scope: ['admin', 'user']
            },
            tags: ['api']
        },
        handler: async (request, h) => {
            const {userService} = request.services();

            if (request.params.id)
                return await userService.getOne(request.params.id);
            else
                return await userService.getAll();
        }
    },
    {
        method: 'DELETE',
        path: '/user/{id}',
        options: {
            tags: ['api'],
            auth: {
                scope: ['admin']
            }
        },
        handler: async (request, h) => {
            const {userService} = request.services();

            return await userService.delete(request.params.id);
        }
    },
    {
        method: 'PATCH',
        path: '/user',
        options: {
            tags: ['api'],
            auth: {
                scope: ['admin']
            },
            validate: {
                payload: Joi.object({
                    id: Joi.number().integer().greater(0).required(),
                    firstName: Joi.string().min(3).example('John').description('Firstname of the user'),
                    lastName: Joi.string().min(3).example('Doe').description('Lastname of the user'),
                    password: Joi.string().min(8).example('DG12#jdS')
                        .regex(new RegExp('^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$'))
                        .description('Password of the user'),
                    username: Joi.string().min(3).example('Josh45853').description('Username of the user'),
                    mail: Joi.string().min(3).example('Josh45853@example.com')
                        .regex(new RegExp('(?:[a-z0-9!#$%&\'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&\'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\\])'))
                        .description('Mail address of the user')
                })
            }
        },
        handler: async (request, h) => {
            const {userService} = request.services();
            const res = await userService.update(request.payload);
            return res ? h.response({text: 'User updated', done: true}).code(200) : Boom.badData('User not found!');
        }
    },
    {
        method: 'POST',
        path: '/user/login',
        options: {
            tags: ['api'],
            validate: {
                payload: Joi.object({
                    password: Joi.string().min(8).example('DG12#jdS')
                        //.regex(new RegExp('^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$'))
                        .description('Password of the user').required(),
                    mail: Joi.string().min(3).example('Josh45853@example.com')
                        //.regex(new RegExp('(?:[a-z0-9!#$%&\'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&\'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\\])'))
                        .description('Mail address of the user').required()
                })
            },
            auth: false
        },
        handler: async (request, h) => {
            const {userService} = request.services();

            let res = await userService.authentication(request.payload);

            if (res !== null) {

                return h.response({text: 'You\'ve been authenticated', token : 'Bearer ' + res}).header('Authorization', 'Bearer ' + res).code(200);
            } else
                return h.response(Boom.unauthorized());
        }
    }
];
